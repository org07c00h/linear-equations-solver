﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Linear
{
    public class Matrix 
    {
        private static double zero = 1e-9;
        private double[,] m_;
        private int n_;
        public Matrix(int n)
        {
            m_ = new double[n, n];
            n_ = n;

        }

        public int n
        {
            get
            {
                return n_;
            }
        }

        public Matrix(string file)
        {
            string[] lines = System.IO.File.ReadAllLines(file);
            n_ = lines.Length;
            m_ = new double[n_, n_];
            int i = 0;
            int j = 0;
            foreach (string l in lines)
            {
                foreach (string num in l.Split(' '))
                {
                    m_[i, j] = double.Parse(num);
                    j++;
                }
                i++;
                j = 0;
            }
        }

        public Matrix(double[,] array, int n)
        {
            m_ = array;
            n_ = n;
        }

        public Matrix Transpose()
        {
            double[,] temp = new double[n_, n_];
            for (int i = 0; i < n_; i++)
            {
                for (int j = 0; j < n_; j++)
                {
                    temp[j, i] = m_[i, j];
                }
            }
            return new Matrix(temp, n_);
        }

        public double this[int i, int j]
        {
            get
            {
                return m_[i, j];
            }

            set
            {
                m_[i, j] = value;
            }
        }
		public static Matrix operator + (Matrix left, Matrix right)
		{
			double[,] tmp = new double[left.n, left.n];
			for (int i = 0; i < left.n; i++) {
				for (int j = 0; j< left.n; j++) {
					tmp [i, j] = left [i, j] + right [i, j];
				}
			}
			return new Matrix (tmp, left.n);
		}
        public static Matrix operator *(Matrix left, Matrix right)
        {
            double[,] temp = new double[left.n_, left.n_];

            for (int i = 0; i < left.n_; i++)
            {
                for (int j = 0; j < left.n_; j++)
                {
                    temp[i, j] = 0;
                    for (int k = 0; k < left.n_; k++)
                    {
                        temp[i, j] += left[i, k] * right[k, j];
                    }
                }
            }
            return new Matrix(temp, left.n_);
        }

        public static Matrix operator / (Matrix left, double num)
        {
            for ( int i = 0;i < left.n_; i++)
            {
                left.MultiplyRow(i, 1.0/num);
            }
            return left;
        }

		public static Matrix operator * (double num, Matrix left)
		{
			for ( int i = 0;i < left.n_; i++)
			{
				left.MultiplyRow(i, num);
			}
			return new Matrix(left.m_, left.n_);
		}

        public static double[] operator * (Matrix left, double[] right)
        {
            double[] result = new double[left.n_];
            for (int i = 0; i < left.n; i++ )
            {
                result[i] = 0;
                for(int j = 0; j < left.n; j++)
                {
                    result[i] += left[i, j] * right[j];
                }
            }
            return result;
        }

        public void DiagDom()
        {
            double max;
            int maxIndex;
            for(int i = 0; i < n_ - 1; i++)
            {
                max = Math.Abs(m_[i, i]);
                maxIndex = i;
                for(int j = 1; j < n_; j++)
                {
                    if (Math.Abs(m_[j, i]) > max)
                    {
                        max = Math.Abs(m_[j, i]);
                        maxIndex = j;
                    }
                }
                if (maxIndex != i)
                {
                    this.SwapRows(maxIndex, i);
                }
            }
        }

        public void MultiplyRow(int n, double num)
        {
            for (int i = 0; i < n_; i++)
            {
                m_[n, i] *= num;
            }
        }

        public void SumRows(int n, int k, double c)
        {
            for (int i = 0; i < n_; i++)
            {
                m_[n, i] += c * m_[k, i];
            }

        }

        public void SwapRows(int n, int k)
        {
            double temp;
            for (int i = 0; i < n_; i++)
            {
                temp = m_[n, i];
                m_[n, i] = m_[k, i];
                m_[k, i] = temp;
            }
        }

        public override string ToString()
        {
            string text = string.Empty;

            for (int i = 0; i < n_; i++)
            {
                for (int j = 0; j < n_; j++)
                {
                    text += m_[i, j];
                    text += "\t";
                }
                text += "\r\n";
            }

            return text;
        }

        
        
        public double Norm()
        {
                List<double> sumList = new List<double>();
                double sum;
                for (int i = 0; i < n_; i++)
                {
                    sum = 0;
                    for (int j = 0; j < n_; j++)
                    {
                        sum += Math.Abs(m_[i, j]);
                    }
                    sumList.Add(sum);
                }

                return sumList.Max();
        }

        public Matrix SubMartix(int row, int col, int num)
        {
            double[,] temp = new double[num, num];

            for (int i = 0; i < num; i++)
            {
                for (int j = 0; j < num; j++)
                {
                    temp[i, j] = m_[row + i, col + j];
                }
            }

            return new Matrix(temp, num);
        }

        public Matrix DeepClone()
        {
            double[,] temp = new double[this.n_, this.n_];
            for (int i = 0; i < this.n_; i++)
            {
                for (int j = 0; j < this.n_; j++)
                {
                    temp[i, j] = this.m_[i, j];
                }
            }
            return new Matrix(temp, this.n_);
        }

		public bool isPositive()
		{
			GaussData data;
			for (int i = 2; i < n - 1; i++) {
				data = Gauss.Solve (this.SubMartix (0, 0, i));
				if (data.det <= 0) {
					return false;
				}
			}

			if (Gauss.Solve (this.DeepClone ()).det <= 0) {
				return false;
			}

			if (this.m_ [0, 0] <= 0) {
				return false;
			}
			return true;

		}

	}
}