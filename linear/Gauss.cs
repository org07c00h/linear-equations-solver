﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linear
{

    public struct GaussData
    {
        public double det;
        public double[] solution;

    };

    public static class Gauss
    {
        private const double zero = 1e-9;

        public static GaussData Solve(Matrix m)
        {
            double[] b = new double[m.n];
            
            for (int i = 0; i < m.n; i++)
            {
                b[i] = 0;
            }

            return Solve(m, b);
        }

        public static GaussData Solve(Matrix m, double[] b0)
        {
			double[] b = new double[b0.Length];
			for (int j = 0; j < b0.Length; j++) {
				b [j] = b0 [j];
			}
            GaussData sol = new GaussData();
            sol.det = 1;
            if (m.n != b.Length)
                throw new Exception("dim");

            int i = 0;

            while (i < m.n )
            {
                if (Math.Abs(m[i, i]) < zero)
                {
                    int k = i + 1;

                    while ((k < m.n) && (Math.Abs(m[k, i]) < zero))
                    {
                        k++;
                    }

                    if (Math.Abs(m[k, i]) < zero)
                    {
                        throw new Exception("special matrix");
                    }
                    else
                    {
                        m.SwapRows(i, k);
                        sol.det *= -1;
                        double temp = b[i];
                        b[i] = b[k];
                        b[k] = temp;
                    }
                }


                b[i] /= m[i, i];
                sol.det *= m[i, i];
                m.MultiplyRow(i, 1.0 / m[i, i]);

                for (int j = 0; j < m.n; j++)
                {
                    if (i != j)
                    {
                        b[j] -= m[j, i] * b[i];
                        m.SumRows(j, i, -m[j, i]);
                    }
                }
                i++;
            }

            sol.solution = b;
            return sol;
        }
    }


}
