﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linear
{
    class Program
    {
        static void Main(string[] args)
        {
			/*double[,] t = new double[7, 7];
			double[] b = new double[7];

			for (int i = 0; i < 7; i++) {
				for (int j = 0; j < 7; j++) {
					if (i == j)
						t [i, j] = 1 + 1e-3 * 5;
					else if (i > j)
						t [i, j] = 0 + 1e-3 * 5;
					else {
						t [i, j] = -1 -1e-3 * 5;
					}
				}
				b[i] = -1;
			}

			b [6] = 1;

			Matrix m = new Matrix (t, 7);
			*/
			/*double[] x = Seidel.Solve (m, b, 1e-2);

			for (int i = 0; i < 7; i++) {
				Console.Write("{0} ",x[i]);
			}
			Console.WriteLine ();
			m = new Matrix (t, 7);
			b = m * x;
			for (int i = 0; i < 7; i++) {
				Console.Write ("{0} ", b[i]);
			}*/

			Matrix m = new Matrix ("/tmp/m.txt");

			double[] b = { 11, 13, 15 };
			double[] x3 = Seidel.Solve (m.DeepClone(), b, 1e-7);
			double[] x1 = SimpleIteration.Solve (m.DeepClone(), b, 1e-7);
			double[] x2 = Gauss.Solve (m.DeepClone(), b).solution;


			Console.Write ("МПИ: ");
			for (int i = 0; i < b.Length; i++) {
				Console.Write ("{0}\t", x1 [i]);
			}
			Console.WriteLine ();
			Console.Write ("Gauss: ");
			for (int i = 0; i < b.Length; i++) {
				Console.Write ("{0}\t", x2 [i]);
			}
			Console.WriteLine ();
			Console.Write ("Seidel: ");
			for (int i = 0; i < b.Length; i++) {
				Console.Write ("{0}\t", x3 [i]);
			}
			Console.WriteLine ();
            Console.ReadKey();

			double[] x = m.DeepClone () * x3;
			for (int i = 0; i < b.Length; i++) {
				Console.Write ("{0}\t", x [i]);
			}
        }
    }
}
