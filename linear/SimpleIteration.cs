﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linear
{
    public static class SimpleIteration
    {
        private const double zero = 1e-9;
		public static double[] Solve(Matrix m, double[] b0, double e)
        {
			double[] b = new double[b0.Length];
			for (int i = 0; i < b0.Length; i++) {
				b [i] = b0 [i];
			}
            double[] x = new double[b.Length];
            double[] xPrev = new double[b.Length];
			if (!m.isPositive () || m.Norm() > 1) {

				b = m.Transpose () * b;
				m = m.Transpose () * m;
			}
            double norm = m.Norm();

            m /= -norm;
            for (int i = 0; i < b.Length; i++)
            {
                b[i] /= norm;
                m[i, i] = 1 + m[i, i];
                xPrev[i] = b[i];
                x[i] = 0;
            }

			int counter = 0;

			norm = m.Norm ();
			double k = Math.Abs (norm / (1 - norm));
			while (Norm(x, xPrev) * k > e)
            {
				counter++;
                double[] xTemp = (m * xPrev);
                for (int i = 0; i < x.Length; i++)
                {
                    xPrev[i] = x[i];
                    x[i] = xTemp[i] + b[i];
                }
            }

			Console.WriteLine ("MPI Iterations: {0}", counter);
			return x;
            

        }

        private static double Norm(double[] x, double[] y)
        {
            double sum = 0;
            for(int i =0; i< x.Length; i++)
            {
                sum += (x[i] - y[i]) * (x[i] - y[i]);
            }
            return Math.Sqrt(sum);
        }
    }
}
