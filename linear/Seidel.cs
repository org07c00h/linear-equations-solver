﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linear
{
    public static class Seidel
    {
        private const double zero = 1e-9;
		public static double[] Solve(Matrix m, double[] b0, double e)
        {
			double[] b = new double[b0.Length];
			for (int j = 0; j < b0.Length; j++) {
				b [j] = b0 [j];
			}


			if ((!m.isPositive ()) || m.Norm () > 1) {
				m.DiagDom ();
				b = m.Transpose () * b;
				m = m.Transpose () * m;
			} else {
				m.DiagDom();
			}

            

            int i = 0;
            while (i < m.n)
            {
                if (Math.Abs(m[i, i]) < zero)
                {
                    int k = i + 1;

                    while ((k < m.n) && (Math.Abs(m[k, i]) < zero))
                    {
                        k++;
                    }

                    if (Math.Abs(m[k, i]) < zero)
                    {
                        throw new Exception("special matrix");
                    }
                    else
                    {
                        m.SwapRows(i, k);
                        double temp = b[i];
                        b[i] = b[k];
                        b[k] = temp;
                    }
                }
                b[i] /= m[i, i];
                m.MultiplyRow(i, -1.0 / m[i, i]);

                m[i, i] = 0;


                i++;
            }

            double[] xk = new double[m.n];
            double[] xk_1 = new double[m.n];

            for(i = 0; i < m.n; i++)
            {
                xk[i]  = 0;
				xk_1 [i] = b[i];
            }
            
			int counter = 0;
            do
            {
				counter++;
				for (i = 0; i < m.n; i++)
				{
					xk[i] = xk_1[i];
				}

                for (i = 0; i < m.n; i++)
                {
                    double sum = 0;
					for (int j = 0; j < m.n; j++)
                    {
						sum += (m[i, j] * xk_1[j]);
                    }
                    
					xk_1[i] = b[i] + sum;
                }

            }
			while (Norm(xk, xk_1) > e);
			Console.WriteLine ("Seidel Iterations: {0}", counter);
			return xk_1;
        }

        private static double Norm(double[] x1, double[] x2)
        {
            double norm = 0;

            for (int i = 0; i < x1.Length; i++ )
            {
				norm +=  (x1 [i] - x2 [i])*(x1 [i] - x2 [i]);
            }

			return Math.Sqrt(norm);
        }
    }
}
